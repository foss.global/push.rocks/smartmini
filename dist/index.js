"use strict";
require("typings-global");
const uglifyJs = require("uglify-js");
class Smartmini {
    /**
     * the constructor for Smartmini
     */
    constructor() { }
    /**
     * minifies JavaScript from a file
     */
    minifyFromJsFile(filePathArg) {
        return uglifyJs.minify(filePathArg);
    }
    /**
     * minifies JavaScript from a string
     */
    minifyFromJsString(stringToMinifyArg) {
        return uglifyJs.minify(stringToMinifyArg, { fromString: true });
    }
}
exports.Smartmini = Smartmini;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsMEJBQXVCO0FBQ3ZCLHNDQUFxQztBQUVyQztJQUVJOztPQUVHO0lBQ0gsZ0JBQWUsQ0FBQztJQUVoQjs7T0FFRztJQUNILGdCQUFnQixDQUFDLFdBQW1CO1FBQ2hDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO0lBQ3ZDLENBQUM7SUFHRDs7T0FFRztJQUNILGtCQUFrQixDQUFDLGlCQUF5QjtRQUN4QyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO0lBQ2pFLENBQUM7Q0FDSjtBQXJCRCw4QkFxQkMifQ==