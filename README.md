# smartmini
uglify js in a nice TypeScript class

## Availabililty
[![npm](https://push.rocks/assets/repo-button-npm.svg)](https://www.npmjs.com/package/smartmini)
[![git](https://push.rocks/assets/repo-button-git.svg)](https://GitLab.com/pushrocks/smartmini)
[![git](https://push.rocks/assets/repo-button-mirror.svg)](https://github.com/pushrocks/smartmini)
[![docs](https://push.rocks/assets/repo-button-docs.svg)](https://pushrocks.gitlab.io/smartmini/)

## Status for master
[![build status](https://GitLab.com/pushrocks/smartmini/badges/master/build.svg)](https://GitLab.com/pushrocks/smartmini/commits/master)
[![coverage report](https://GitLab.com/pushrocks/smartmini/badges/master/coverage.svg)](https://GitLab.com/pushrocks/smartmini/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/smartmini.svg)](https://www.npmjs.com/package/smartmini)
[![Dependency Status](https://david-dm.org/pushrocks/smartmini.svg)](https://david-dm.org/pushrocks/smartmini)
[![bitHound Dependencies](https://www.bithound.io/github/pushrocks/smartmini/badges/dependencies.svg)](https://www.bithound.io/github/pushrocks/smartmini/master/dependencies/npm)
[![bitHound Code](https://www.bithound.io/github/pushrocks/smartmini/badges/code.svg)](https://www.bithound.io/github/pushrocks/smartmini)
[![TypeScript](https://img.shields.io/badge/TypeScript-2.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%206.x.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Usage
Use TypeScript for best in class instellisense.

[![npm](https://push.rocks/assets/repo-header.svg)](https://push.rocks)
